=begin
The answer is 104743
The calculation took 12.119918sec

Approach:

This is by far not the most optimal way to solve this, but considering it only took 9-11 secs, I didnt bother optimizing it any further

Basically, divide every number by a list of known prime numbers and check for remainder. If remainder is 0 during the iteration, automatically move to next iteration. If by the end of the iterations none of the remainders are 0, then you have a prime number. To further make it faster i was thinking of maybe introducing a blacklist that can be used to skip numbers before doing any remainder calculations (eg. if you know that number 25 is divisible by 5, then 25+5 = 30 is also a non-prime therefore no need to check it)

=end


primesArray = [2]
currentNumber = 2

#a function that calculates the time it takes a passed block to run
def runCode &block
  startTime = Time.now
  answer = block.call
  duration = Time.now - startTime
  puts "The answer is " + answer, "The calculation took " + duration.to_s + "sec"
end

runCode do
  while primesArray.length < 10001 do
    currentNumber = currentNumber + 1
    remainder = 0
    primesArray.each do |prime|
      remainder = currentNumber % prime
      break if prime > (Math.sqrt(currentNumber)) || remainder.zero?
    end
    next if remainder.zero? 
    primesArray << currentNumber
  end
  primesArray.last.to_s
end














