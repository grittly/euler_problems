
def palindrome? num
  num.to_s.reverse == num.to_s
  
end


product = 0
number1=0
number2=0

(101..999).reverse_each do |num1|
  (num1..999).reverse_each do |num2|
  	if num1*num2 > product && palindrome?(num1*num2)
  		product = num1*num2
  		number1=num1
  		number2=num2
  	end
  end
end


puts "The largest palindrome product is #{product} by multiplying #{number1} and #{number2}"


















